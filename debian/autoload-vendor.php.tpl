<?php
  require_once 'HTMLPurifier.auto.php';
  require_once 'League/HTMLToMarkdown/autoload.php';
  require_once 'Psr/Log/autoload.php';
  require_once 'Sabre/autoload.php';
//require_once 'simplepie/autoloader.php';
  require_once 'smarty3/bootstrap.php';
//require_once 'Symfony/Component/OptionsResolver/autoload.php';
//require_once 'Symfony/Component/Process/autoload.php';
  require_once 'Symfony/Polyfill/Ctype/autoload.php';
  require_once 'Text/LanguageDetect.php';

// @codingStandardsIgnoreFile
// @codeCoverageIgnoreStart
// this is an autogenerated file - do not edit
spl_autoload_register(
    function($class) {
        static $classes = null;
        if ($classes === null) {
            $classes = array(
                ___CLASSLIST___
            );
        }
        $cn = strtolower($class);
        if (isset($classes[$cn])) {
            require ___BASEDIR___ $classes[$cn];
        }
    },
    true,
    false
);
// @codeCoverageIgnoreEnd
