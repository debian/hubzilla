#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

%:
	dh $@ --buildsystem=phppear --with phpcomposer

override_dh_clean:
	dh_clean
	## Remove Files-Excluded (when built from checkout or non-DFSG tarball):
	$(RM) -rv `perl -0nE 'say $$1 if m{^Files\-Excluded\:\s*(.*?)(?:\n\n|Files:|Comment:)}sm;' debian/copyright`
	find -type f -name '*.pyc' -delete -printf 'removed %p\n'
	find -type f -name '.eslintrc*' -delete -printf 'removed %p\n'
	$(RM) -r doc/html

vendor/autoload.php:
	phpab --output $@ \
              --template debian/autoload-vendor.php.tpl \
              --exclude 'vendor/composer/*' \
              --exclude '**/TestUtil.php' \
            vendor Zotlabs \
        ;

override_dh_auto_build: vendor/autoload.php
	dh_auto_build
	## build "doc/html":
	doxygen util/Doxyfile
	find doc/html -type f -name '*graph*.md5' -delete

#override_dh_auto_test:
#	dh_auto_test
#ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
#	phpunit --verbose --bootstrap vendor/autoload.php tests/unit/
#endif

override_dh_missing:
	dh_missing --fail-missing

override_dh_install:
	dh_install \
            -Xlicense.txt -XLICENSE.txt -XLICENSE -XLICENSE.md -XCOPYING -Xgpl.txt \
            -XLICENSE.GPL -XLICENSE.LGPL -XLICENSE.TXT \
            -X.gitignore -X.gitmodules -X.gitattributes -X.DS_Store -X.swf \
            -Xagpl-3.0.txt -Xagpl-3.0.exception.txt \
            -XCHANGELOG.TXT -Xchangelog.txt -X/CHANGELOG.md -X/changelog.md \
            -X/bower.json -X/Gruntfile.js -X/package.json -X/composer.json -X/Makefile \
            -X/backbone-forms/scripts/build -Xicons.psd
	## executable-not-elf-or-script
	chmod -R -c a-x+X debian/*/usr/share
	
	## already installed to "/usr/share/doc/hubzilla/":
	$(RM) -r debian/hubzilla/usr/share/hubzilla/doc/html
	
	## missing-sources:
	$(RM) -r debian/hubzilla/usr/share/hubzilla/library/ace
	
	## useless:
	$(RM) -r debian/hubzilla/usr/share/hubzilla/vendor/composer
	
	## system packages:
	$(RM) -r \
            debian/hubzilla/usr/share/hubzilla/library/cryptojs \
            debian/hubzilla/usr/share/hubzilla/library/HTML5 \
            debian/hubzilla/usr/share/hubzilla/library/symfony/options-resolver \
            debian/hubzilla/usr/share/hubzilla/library/symfony/process \
        ;

override_dh_link:
	## we need to call dh_linktree before dh_link, not after like it
	## happens by default in "--with linktree" mode.
	dh_linktree
	
	## link minified .CSS and .JS with to their uncompressed versions:
	find $(CURDIR)/debian/hubzilla/usr -type f -name "*[-.]min.css" -or -name "*[-.]min.js" | \
        while read MF; do \
            UF="$$(echo $${MF} | perl -pE 's{[\.-]min\.(js|css)}{.$$1};')" ;\
            if [ -f "$${UF}" -o -h "$${UF}" ]; then \
                ln -svfr "$${UF}" "$${MF}" ;\
            else \
                UF="$$(echo $${UF} | perl -pE 's{/\K([^/]+)\Z}{../$$1};')" ;\
                if [ -f "$${UF}" -o -h "$${UF}" ]; then \
                    ln -svfr "$${UF}" "$${MF}" ;\
                    continue ;\
                else \
                    UF="$$(echo $${UF} | perl -pE 's{/\K([^/]+)\Z}{src/$$1};')" ;\
                    [ -f "$${UF}" -o -h "$${UF}" ] && ln -svfr "$${UF}" "$${MF}" ;\
                fi ;\
            fi ;\
        done
	dh_link
	## remove empty directories:
	find $(CURDIR)/debian/hubzilla/usr -type d -empty -delete -printf 'removed %p\n'
